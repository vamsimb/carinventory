function problem6(inventory) {
    let cars =[];
    for(let model = 0; model < inventory.length; model++) {
        let carMake = inventory[model]['car_make']
        if(carMake === "BMW" || carMake === "Audi") {
            cars.push(inventory[model]);
        }
    }
    return cars
}

module.exports = problem6;