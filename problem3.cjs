function problem3(inventory) {
  let carModel=[]
  for(let carID = 0; carID < inventory.length; carID++) {
    carModel.push(inventory[carID]['car_model'])
  }
  return carModel.sort();
}

module.exports = problem3;