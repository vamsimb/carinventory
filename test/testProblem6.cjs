const problem6 = require('../problem6.cjs');
const inventory = require('../inventory.cjs');

const result = problem6(inventory);

console.log(JSON.stringify(result));
