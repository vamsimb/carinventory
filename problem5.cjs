function problem5(inventory) {
  let olderCars =[];
  let carYears=[];
  for(let below = 0; below < inventory.length; below++) {
    carYears.push(inventory[below]['car_year']);
  }
  
  for(let below = 0; below < carYears.length; below++) {
    if(carYears[below] < 2000){
        olderCars.push(carYears[below]);
    }
 }
 return "[" + olderCars +  "]" + "\n"  + olderCars.length + " cars are older than the year 2000";
}

module.exports = problem5;