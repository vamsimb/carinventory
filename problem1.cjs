function problem1(inventory =[],carID) {
  let notValid = []
  if(inventory.length==0 || carID ==  null || Array.isArray(inventory) == false || typeof(carID) != 'number') {
    return notValid;
  }
  for(let carNumber =0; carNumber < inventory.length; carNumber++) {
    if(inventory[carNumber].id == carID) {
      return inventory[carNumber];
    }
  }
  return notValid;
}

module.exports = problem1;