function problem4(inventory) {
  let carYears=[];
  for(let year = 0; year < inventory.length; year++) {
    carYears.push(inventory[year]['car_year']);
  }
  return carYears;
}

module.exports = problem4;